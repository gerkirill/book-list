define(['Book'], function(Book) {
    return Backbone.Collection.extend({
        model: Book,
        localStorage: new Backbone.LocalStorage('books')
    });
});
