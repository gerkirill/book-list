define(['BooksTableRow'], function(BooksTableRow) {
    return Backbone.View.extend({
        initialize: function() {
            this.render();
            this.listenTo(this.collection, 'all', this.render);
        },
        render: function() {
            // var source = $('#list-of-books-tmp').html();
            // this.template = Handlebars.compile(source);
            // this.$el.html(this.template(this.collection.toJSON()));
            this.$el.html('<table>');
            this.collection.forEach(function(model) {
                var row = new BooksTableRow({model: model});
                this.$el.append(row.render().el);
            }.bind(this));
            return this;
        }
    });
});
