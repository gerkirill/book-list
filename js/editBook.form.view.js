define(function() {
    return Backbone.View.extend({
        events: {
            'click .open-add-book-form': 'showForm',
            'click .cancel-btn': 'toggleForm',
            'submit #edit-book-form': 'saveBook'
        },
        initialize: function() {
            this.render();
        },
        render: function() {
            var source = $('#edit-book-form-tmp').html();
            var template = Handlebars.compile(source);
            var html = template({formHeader: 'Create or update book'});
            this.$el.find('#edit-book-form').html(html);
            return this;
        },
        toggleForm: function() {
            this.$el.find('#edit-book-form').toggleClass('show-form');
        },
        showForm: function() {
            this.$el.find('#edit-book-form').addClass('show-form');
            this.$el.find('input[name="bookTitle"]').val('');
            this.$el.find('input[name="bookAuthor"]').val('');
            this.editBookId = undefined;
        },
        saveBook: function(event) {
            event.preventDefault();
            var tmpBook = {
                title: this.$el.find('input[name="bookTitle"]').val(),
                author: this.$el.find('input[name="bookAuthor"]').val()
            };
            this.trigger('save-book', tmpBook, this.editBookId);
        }
    });
});
