require.config({
    paths: {
        'Book': '/js/book.model',
        'ListOfBooks': 'js/books.collection',
        'BooksTable': 'js/books.table.view',
        'BooksTableRow': 'js/books.table-row.view',
        'EditBookForm': 'js/editBook.form.view'
    }
});

require(
    ['Book', 'ListOfBooks', 'BooksTable', 'EditBookForm'],
    function(Book, ListOfBooks, BooksTable, EditBookForm) {
        var list = new ListOfBooks();
        list.fetch();

        var booksTable = new BooksTable({
            el: $('#books-table'),
            collection: list
        });

        var editBookForm = new EditBookForm({
            el: $('#edit-book')
        });

        editBookForm.listenTo(booksTable, 'edit-book', function(book) {
            if (book) {
                if(!this.$el.find('#edit-book-form').hasClass('show-form')) {
                    this.toggleForm();
                }
                this.editBookId = book.id;
                this.$el.find('input[name="bookTitle"]').val(book.get('title'));
                this.$el.find('input[name="bookAuthor"]').val(book.get('author'));
            }
        });

        list.listenTo(editBookForm, 'save-book', function(book, editBookId) {
            if (editBookId) {
                this.localStorage.destroy(this.get(editBookId));
                this.localStorage.update(this.get(editBookId).set(book));
            } else {
                this.create(book);
            }
            editBookForm.toggleForm();
        });
    }
);
